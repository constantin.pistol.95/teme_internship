### 1 - pregatirea clusterului

```
$ minikube start
$ minikube status

minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured
timeToStop: Nonexistent

$ kubectl get nodes
```

### 2.1 - deploy nginx container

```
$ kubectl apply -f deployment.yaml

deployment.apps/nginx-deployment-tema created

$ kubectl get deployments

NAME                    READY   UP-TO-DATE   AVAILABLE   AGE
nginx                   1/1     1            1           5d12h
nginx-deployment        1/1     1            1           4d11h
nginx-deployment-tema   1/1     1            1           24h

$ kubectl describe deployment nginx-deployment-tema

Name:                   nginx-deployment-tema
Namespace:              default
CreationTimestamp:      Mon, 15 Feb 2021 22:25:02 +0200
Labels:                 app=nginx
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=nginx
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx
  Containers:
   nginx:
    Image:        nginx:1.19.6
    Port:         80/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-tema-76ccf9dd9d (1/1 replicas created)
Events:          <none>
```

### 2.2 - deploy nginx service

```
$ kubectl apply -f service.yaml

service/nginx-service-tema created

$ kubectl get svc

NAME                 TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)        AGE
kubernetes           ClusterIP   10.96.0.1        <none>        443/TCP        4d23h
my-service           NodePort    10.108.141.55    <none>        80:31669/TCP   3d10h
nginx-service-tema   NodePort    10.100.190.243   <none>        80:30572/TCP   39s
ngnix                NodePort    10.101.137.126   <none>        80:31918/TCP   4d11h

$ kubectl describe svc nginx-service-tema 

Name:                     nginx-service-tema
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=nginx
Type:                     NodePort
IP:                       10.100.190.243
Port:                     <unset>  80/TCP
TargetPort:               80/TCP
NodePort:                 <unset>  30572/TCP
Endpoints:                172.17.0.2:80,172.17.0.3:80,172.17.0.6:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>

$ minikube service nginx-service-tema

$ curl  http://127.0.0.1:59723
```

### 3.1 - Job

- surse:
     -https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/
    - https://opensource.com/article/20/11/kubernetes-jobs-cronjobs
    - https://kubernetes.io/docs/concepts/workloads/controllers/job/

```
$ kubectl apply -f job.yaml

job.batch/env-printer-job-tema created

$ kubectl get jobs

NAME                   COMPLETIONS   DURATION   AGE
env-printer-job        1/1           22s        3d19h
env-printer-job-tema   1/1           5s         16s

$ kubectl get pods

NAME                                     READY   STATUS      RESTARTS   AGE
env-printer-job-sqb8k                    0/1     Completed   0          3d19h
env-printer-job-tema-2cppk               0/1     Completed   0          24s
nginx-7848d4b86f-cprbp                   1/1     Running     2          4d21h
nginx-deployment-76ccf9dd9d-rg9kn        1/1     Running     1          3d20h
nginx-deployment-tema-76ccf9dd9d-8x4wx   1/1     Running     0          10h

$ kubectl logs env-printer-job-tema-2cppk # numele podului

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=env-printer-job-tema-2cppk
KUBERNETES_SERVICE_HOST=10.96.0.1
MY_SERVICE_PORT_80_TCP=tcp://10.108.141.55:80
MY_SERVICE_PORT_80_TCP_PORT=80
NGNIX_SERVICE_HOST=10.101.137.126
NGNIX_SERVICE_PORT=80
KUBERNETES_PORT_443_TCP_PROTO=tcp
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
NGNIX_PORT_80_TCP_PROTO=tcp
NGNIX_PORT_80_TCP_ADDR=10.101.137.126
KUBERNETES_SERVICE_PORT=443
KUBERNETES_PORT_443_TCP_PORT=443
MY_SERVICE_PORT=tcp://10.108.141.55:80
MY_SERVICE_PORT_80_TCP_ADDR=10.108.141.55
MY_SERVICE_SERVICE_HOST=10.108.141.55
MY_SERVICE_SERVICE_PORT=80
NGINX_SERVICE_TEMA_PORT=tcp://10.100.190.243:80
NGINX_SERVICE_TEMA_PORT_80_TCP_PORT=80
NGNIX_PORT=tcp://10.101.137.126:80
NGNIX_PORT_80_TCP=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_SERVICE_PORT=80
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
MY_SERVICE_PORT_80_TCP_PROTO=tcp
NGINX_SERVICE_TEMA_PORT_80_TCP_ADDR=10.100.190.243
KUBERNETES_PORT=tcp://10.96.0.1:443
NGNIX_PORT_80_TCP_PORT=80
NGINX_SERVICE_TEMA_SERVICE_HOST=10.100.190.243
NGINX_SERVICE_TEMA_PORT_80_TCP=tcp://10.100.190.243:80
NGINX_SERVICE_TEMA_PORT_80_TCP_PROTO=tcp
HOME=/root
```

### 3.2 - CronJob

```
$ kubectl apply -f cron-job.yaml

cronjob.batch/env-printer-cronjob-tema created

$ kubectl get cronjobs

NAME                       SCHEDULE      SUSPEND   ACTIVE   LAST SCHEDULE   AGE
env-printer-cronjob-tema   */1 * * * *   False     0        <none>          27s

$ kubectl get pods

NAME                                        READY   STATUS      RESTARTS   AGE
env-printer-cronjob-tema-1613458800-zpgd7   0/1     Completed   0          80s
env-printer-cronjob-tema-1613458860-gz45t   0/1     Completed   0          19s
env-printer-job-sqb8k                       0/1     Completed   0          3d20h
env-printer-job-tema-2cppk                  0/1     Completed   0          22m
nginx-7848d4b86f-cprbp                      1/1     Running     2          4d22h
nginx-deployment-76ccf9dd9d-rg9kn           1/1     Running     1          3d21h
nginx-deployment-tema-76ccf9dd9d-8x4wx      1/1     Running     0          10h

$ kubectl logs env-printer-cronjob-tema-1613458860-gz45t # numele podului

PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
HOSTNAME=env-printer-cronjob-tema-1613458860-gz45t
NGNIX_SERVICE_HOST=10.101.137.126
NGINX_SERVICE_TEMA_PORT_80_TCP_PORT=80
NGINX_SERVICE_TEMA_PORT_80_TCP_ADDR=10.100.190.243
KUBERNETES_PORT_443_TCP_PROTO=tcp
MY_SERVICE_PORT_80_TCP=tcp://10.108.141.55:80
MY_SERVICE_PORT_80_TCP_PORT=80
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
MY_SERVICE_SERVICE_HOST=10.108.141.55
NGNIX_PORT_80_TCP_ADDR=10.101.137.126
NGINX_SERVICE_TEMA_PORT_80_TCP=tcp://10.100.190.243:80
NGNIX_PORT_80_TCP_PORT=80
KUBERNETES_SERVICE_PORT=443
NGNIX_PORT_80_TCP_PROTO=tcp
NGNIX_SERVICE_PORT=80
NGNIX_PORT=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_PORT=tcp://10.100.190.243:80
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
MY_SERVICE_SERVICE_PORT=80
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT=tcp://10.96.0.1:443
MY_SERVICE_PORT_80_TCP_PROTO=tcp
MY_SERVICE_PORT_80_TCP_ADDR=10.108.141.55
NGNIX_PORT_80_TCP=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_SERVICE_HOST=10.100.190.243
NGINX_SERVICE_TEMA_SERVICE_PORT=80
NGINX_SERVICE_TEMA_PORT_80_TCP_PROTO=tcp
KUBERNETES_SERVICE_HOST=10.96.0.1
MY_SERVICE_PORT=tcp://10.108.141.55:80
KUBERNETES_SERVICE_PORT_HTTPS=443
HOME=/root
```

### 4 - ConfigMap

- surse:
    - https://phoenixnap.com/kb/kubernetes-configmap-create-and-use
    - https://kubernetes.io/docs/tasks/configure-pod-container/configure-pod-configmap/

- crearea ConfigMap-ului

```
$ kubectl create configmap configmap-tema --from-literal appName=tema --from-literal appEnv=test
configmap/configmap-tema created

$ kubectl get configmaps configmap-tema -o yaml
apiVersion: v1
data:
  appEnv: test
  appName: tema
kind: ConfigMap
metadata:
  creationTimestamp: "2021-02-16T07:59:19Z"
  managedFields:
  - apiVersion: v1
    fieldsType: FieldsV1
    fieldsV1:
      f:data:
        .: {}
        f:appEnv: {}
        f:appName: {}
    manager: kubectl-create
    operation: Update
    time: "2021-02-16T07:59:19Z"
  name: configmap-tema
  namespace: default
  resourceVersion: "81325"
  uid: 8e391fa8-ef9d-4ceb-a0af-fb158cd42f23
```

- crearea Pod-ului

```
$ kubectl apply -f configmap-pod.yaml
pod/configmap-pod-tema created

$ kubectl get pods
NAME                                        READY   STATUS      RESTARTS   AGE
configmap-pod-tema                          0/1     Completed   0          7s
env-printer-cronjob-tema-1613495160-lh5cr   0/1     Completed   0          3h22m
env-printer-cronjob-tema-1613495220-lfx4s   0/1     Completed   0          3h21m
env-printer-cronjob-tema-1613495280-rk2lz   0/1     Completed   0          3h20m
env-printer-job-sqb8k                       0/1     Completed   0          4d9h
env-printer-job-tema-2cppk                  0/1     Completed   0          13h
nginx-7848d4b86f-cprbp                      1/1     Running     2          5d11h
nginx-deployment-76ccf9dd9d-rg9kn           1/1     Running     1          4d10h
nginx-deployment-tema-76ccf9dd9d-8x4wx      1/1     Running     0          24h

$ kubectl logs configmap-pod-tema
KUBERNETES_PORT=tcp://10.96.0.1:443
KUBERNETES_SERVICE_PORT=443
MY_SERVICE_PORT_80_TCP=tcp://10.108.141.55:80
HOSTNAME=configmap-pod-tema
SHLVL=1
HOME=/root
NGNIX_PORT_80_TCP=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_PORT_80_TCP=tcp://10.100.190.243:80
APP_NAME=tema
MY_SERVICE_SERVICE_HOST=10.108.141.55
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
NGNIX_SERVICE_HOST=10.101.137.126
NGINX_SERVICE_TEMA_SERVICE_HOST=10.100.190.243
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT_443_TCP_PROTO=tcp
MY_SERVICE_SERVICE_PORT=80
MY_SERVICE_PORT=tcp://10.108.141.55:80
NGNIX_PORT=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_PORT=tcp://10.100.190.243:80
NGNIX_SERVICE_PORT=80
NGINX_SERVICE_TEMA_SERVICE_PORT=80
MY_SERVICE_PORT_80_TCP_ADDR=10.108.141.55
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
KUBERNETES_SERVICE_PORT_HTTPS=443
PWD=/
MY_SERVICE_PORT_80_TCP_PORT=80
KUBERNETES_SERVICE_HOST=10.96.0.1
APP_ENV=test
NGNIX_PORT_80_TCP_ADDR=10.101.137.126
MY_SERVICE_PORT_80_TCP_PROTO=tcp
NGINX_SERVICE_TEMA_PORT_80_TCP_ADDR=10.100.190.243
NGINX_SERVICE_TEMA_PORT_80_TCP_PORT=80
NGNIX_PORT_80_TCP_PORT=80
NGNIX_PORT_80_TCP_PROTO=tcp
NGINX_SERVICE_TEMA_PORT_80_TCP_PROTO=tcp
```

### 5 - Secret

- sursa:
    - https://kubernetes.io/docs/concepts/configuration/secret/
    - https://opensource.com/article/19/6/introduction-kubernetes-secrets-and-configmaps 

- crearea secretului:

```
$ echo -n 'constantinKubernetes' | base64
Y29uc3RhbnRpbkt1YmVybmV0ZXM=

$  echo -n 'parolafoartesecreta0123' | base64
cGFyb2xhZm9hcnRlc2VjcmV0YTAxMjM=

$ kubectl apply -f secret.yaml
secret/secret-tema created

$ kubectl get secrets
NAME                  TYPE                                  DATA   AGE
default-token-g8rnl   kubernetes.io/service-account-token   3      5d23h
secret-tema           Opaque                                2      15s

$ kubectl describe secret secret-tema
Name:         secret-tema
Namespace:    default
Labels:       <none>
Annotations:  <none>

Type:  Opaque

Data
====
password:  23 bytes
username:  20 bytes

$ kubectl get secret secret-tema -o jsonpath='{.data.password}' | base64 --decode

parolafoartesecreta0123

$ kubectl get secret secret-tema -o jsonpath='{.data.username}' | base64 --decode

constantinKubernetes
```

- crearea Pod-ului:

```
$ kubectl apply -f secret-pod.yaml

pod/secret-pod-tema created

$ kubectl get pods

NAME                                        READY   STATUS      RESTARTS   AGE
configmap-pod-tema                          0/1     Completed   0          38m
env-printer-cronjob-tema-1613495160-lh5cr   0/1     Completed   0          4h1m
env-printer-cronjob-tema-1613495220-lfx4s   0/1     Completed   0          4h
env-printer-cronjob-tema-1613495280-rk2lz   0/1     Completed   0          3h59m
env-printer-job-sqb8k                       0/1     Completed   0          4d10h
env-printer-job-tema-2cppk                  0/1     Completed   0          14h
nginx-7848d4b86f-cprbp                      1/1     Running     2          5d12h
nginx-deployment-76ccf9dd9d-rg9kn           1/1     Running     1          4d11h
nginx-deployment-tema-76ccf9dd9d-8x4wx      1/1     Running     0          24h
secret-pod-tema                             0/1     Completed   0          10s

$ kubectl logs secret-pod-tema

KUBERNETES_PORT=tcp://10.96.0.1:443
KUBERNETES_SERVICE_PORT=443
MY_SERVICE_PORT_80_TCP=tcp://10.108.141.55:80
HOSTNAME=secret-pod-tema
SHLVL=1
HOME=/root
NGNIX_PORT_80_TCP=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_PORT_80_TCP=tcp://10.100.190.243:80
MY_SERVICE_SERVICE_HOST=10.108.141.55
USERNAME=constantinKubernetes
KUBERNETES_PORT_443_TCP_ADDR=10.96.0.1
NGNIX_SERVICE_HOST=10.101.137.126
NGINX_SERVICE_TEMA_SERVICE_HOST=10.100.190.243
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
KUBERNETES_PORT_443_TCP_PORT=443
KUBERNETES_PORT_443_TCP_PROTO=tcp
MY_SERVICE_SERVICE_PORT=80
MY_SERVICE_PORT=tcp://10.108.141.55:80
NGNIX_SERVICE_PORT=80
NGNIX_PORT=tcp://10.101.137.126:80
NGINX_SERVICE_TEMA_SERVICE_PORT=80
NGINX_SERVICE_TEMA_PORT=tcp://10.100.190.243:80
MY_SERVICE_PORT_80_TCP_ADDR=10.108.141.55
KUBERNETES_SERVICE_PORT_HTTPS=443
KUBERNETES_PORT_443_TCP=tcp://10.96.0.1:443
PWD=/
MY_SERVICE_PORT_80_TCP_PORT=80
KUBERNETES_SERVICE_HOST=10.96.0.1
MY_SERVICE_PORT_80_TCP_PROTO=tcp
NGNIX_PORT_80_TCP_ADDR=10.101.137.126
NGINX_SERVICE_TEMA_PORT_80_TCP_ADDR=10.100.190.243
PASSWORD=parolafoartesecreta0123
NGNIX_PORT_80_TCP_PORT=80
NGINX_SERVICE_TEMA_PORT_80_TCP_PORT=80
NGNIX_PORT_80_TCP_PROTO=tcp
NGINX_SERVICE_TEMA_PORT_80_TCP_PROTO=tcp
```

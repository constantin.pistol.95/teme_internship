# Docker Homework

## Ex. 1

Create a docker image:
- use ubuntu:latest as a base
- provision it with adoptopenjdk 11 (https://adoptopenjdk.net/installation.html)
- clone https://github.com/spring-projects/spring-petclinic and build the application (instructions found on the git page - use -DskipTests=true)
- make the entrypoint start the application
- start a container with this image and see that it works when accessing localhost:8080

```
docker build -t app:tema1 -f Dockerfile1 .
docker run -p 8080:8080 app:tema1 java -jar target/*.jar
```

## Ex. 2

Configure the application so that it uses a mysql container as its database. You'll have to use mysql:latest for this, and configure the container as per the git page used in the previous task. For this to work, the application must run with the mysql spring profile (--spring.profiles.active=mysql in the java -jar command), and it has to have the MYSQL_URL env variable equal to jdbc:mysql://mysql_container:port/petclinic. Start the mysql container first (wait for the db to finish configuring), and then the java one - make sure they're able to communicate.

```
docker build -t app:tema1 -f Dockerfile1 . # aplicatie
docker build -t db:tema1 -f Dockerfile2 .  # database

# pentru ridicat containerele folosind imaginile create si a le pune in aceeasi retea:
docker network create tema #creeaza o retea, altfel nu se vad containerele intre ele
docker run --network=tema -d --name t2 -e MYSQL_USER=petclinic -e MYSQL_PASSWORD=petclinic -e MYSQL_ROOT_PASSWORD=root -e MYSQL_DATABASE=petclinic -p 3306:3306 db:tema1 #pentru database
docker run --network=tema --name t1 -p 8080:8080 app:tema1 java -jar -Dspring.profiles.active=mysql target/*.jar #pentru aplicatie
```

-------------------------------------

# Ex. 3

OPTIONAL - create a docker compose file to automate this configuration. To control the startup of the containers, read https://docs.docker.com/compose/startup-order/ and use wait-for-it.sh.

```
docker build -t app:tema3 -f Dockerfile3 .
docker-compose up
docker-compose down
```

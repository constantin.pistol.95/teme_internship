# Logging Homework

## Task

You have received a support request from a developer. It sounds like this:
"Hello, DevOps,
I tried deploying the backend service and the deployment job says 'cannot connect to qa-database:5432'.
Can you please check? Thanks!"

You know that the database runs on the 5432 port (PostgreSQL database), and the backend server resides on a separate network (services and databases are separated). You can also assume that you have logging and monitoring tools at your disposal.

How would you approach this situation? What would you check first, and how would you proceed?

## Pasi de investigare

**Primul pas : verificarea logurilor actuale (din momentul aparitiei erorii)**

Ne uitam dupa eroarea exacta in speranta ca ne-ar putea da mai multe detalii.
Developerul spune ca nu se poate conecta la baza de date, iar acest lucru ar putea fi din mai multe cauze:
- introducerea username ului sau a parolei gresite
- foloseste alt url sau un port gresit pentru conectarea la baza de date (putem sa il intrebam direct cum se conecteaza)
- daca descoperim ca foloseste ceva gresit il rugam sa il corecteze si sa reincerce 

**Pasul 2: ne uitam la logurile din urma, verificand daca eroarea a mai aparut intr o zi anterioara**
 
Ne uitam mai intai pe logurile aplicatiei sau tool ul de monitorizare (ex.kibana) pentru a vedea daca exista vreo problema de conexiune dintre aplicatie si baza de date.

Daca aplicatia nu se poate conecta la baza de date inseamna ca este o problema de conexiune de retea. De exemplu:
- un firewall blocheaza accesul
- s-a inchis portul 5432 din greseala
- serviciul de PostgreSQL nu este pornit
- etc

**Pasul 3: ne uitam pe aplicatie**

Verificam daca s-a schimbat ceva in codul aplicatiei cu privire la modalitatea de conectare la baza de date.

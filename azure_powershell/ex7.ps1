# Connect to Azure Portal

Connect-AzAccount

# Select subscription

$subscriptionId="..."
Select-AzSubscription -SubscriptionID $subscriptionId
Set-AzContext $subscriptionId

# ResourceGroup and WebApp

$location="West Europe"
$resourceGroupName="temaAzurePowershell"
$webAppServicePlanName="temaWebAppServicePlan"
$webAppName="temaWebApp"

## Create ResourceGroup
New-AzResourceGroup -Name $resourceGroupName -Location $location

## Create WebApp Service Pan
New-AzAppServicePlan -ResourceGroupName $resourceGroupName -Name $webAppServicePlanName -Location $location -Tier Standard

## Create WebApp

New-AzWebApp -ResourceGroupName $resourceGroupName -Name $webAppName -Location $location -AppServicePlan $webAppServicePlanName


# Functions and StorageAccount

$storageName="temastoragename"
$functionNameC="temafunctienamec"
$servicePlanName="temaserviceplanname"
$functionNameSP="temafunctienamesplan"

## Create StorageAccount
New-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageName -SkuName Standard_LRS -Location $location

## consumption
New-AzFunctionApp -Name $functionNameC -ResourceGroupName $resourceGroupName -StorageAccount $storageName -Runtime PowerShell -FunctionsVersion 3 -Location $location

## service plan hosted

New-AzAppServicePlan -ResourceGroupName $resourceGroupName -Name $servicePlanName -Location $location -Tier "Basic"

New-AzFunctionApp -Name $functionNameSP -ResourceGroupName $resourceGroupName -PlanName $servicePlanName -StorageAccount $storageName -Runtime PowerShell


# KeyVault

$keyVaultName="temakeyvault"
$email="..."
$secureString="hVFkk965BuUv"
$secretName="ExamplePassword"

New-AzKeyVault -Name $keyVaultName -ResourceGroupName $resourceGroupName -Location $location

# dam permisiuni pentru a putea modifica resursa
Set-AzKeyVaultAccessPolicy -VaultName $keyVaultName -UserPrincipalName $email -PermissionsToSecrets get,set,delete

# extragem secretul
$secretvalue = ConvertTo-SecureString $secureString -AsPlainText -Force
$secret = Set-AzKeyVaultSecret -VaultName $keyVaultName -Name $secretName -SecretValue $secretvalue

$secret = Get-AzKeyVaultSecret -VaultName $keyVaultName -Name $secretName
$ssPtr = [System.Runtime.InteropServices.Marshal]::SecureStringToBSTR($secret.SecretValue)
try {
   $secretValueText = [System.Runtime.InteropServices.Marshal]::PtrToStringBSTR($ssPtr)
} finally {
   [System.Runtime.InteropServices.Marshal]::ZeroFreeBSTR($ssPtr)
}
Write-Output $secretValueText


# Cleanup
Remove-AzResourceGroup -Name $resourceGroupName

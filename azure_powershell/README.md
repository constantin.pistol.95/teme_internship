# Tema Azure PowerShell

## Ex 7 (ex7.ps1)

Using PowerShell create a script that creates the following resources (along with their dependencies):
- Resource group
- Web App
- 2x Function Apps (1 consumption, 1 Service Plan Hosted)
- Storage Account
- Key Vault

## Surse

- Resource group, Storage Account and Web App:
    - https://gist.github.com/chadmcrowell/cbdacfee9fedbdd7a5c517f88ee9fa68
- Functions:
    - https://docs.microsoft.com/en-us/azure/azure-functions/create-first-function-cli-powershell?tabs=azure-powershell%2Cbrowser
    - https://thegreenerman.medium.com/create-an-appservice-with-azure-powershell-27c3823702ec
- Key Vault:
    - https://docs.microsoft.com/en-us/azure/key-vault/general/quick-create-powershell
    - https://docs.microsoft.com/en-us/azure/key-vault/secrets/quick-create-powershell

## Ex 8

## Ex 9

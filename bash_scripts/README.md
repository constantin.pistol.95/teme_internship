Ex.1  am adaugat comanda mkdir in script si am verificat ca scriptul are argumente inainte de a executa comanda.
Pentru a executa scriptul am folosit comanda ./script1.sh nume_folder , am testat si varianta fara parametri ./script1.sh care imi afiseaza "no directory name"

Ex.2  pentru script2 am folosit comanda de aici https://stackoverflow.com/questions/5475905/linux-delete-file-with-size-0 si am modificat o conform enuntului, pentru a sterge doar fisierele

Ex.3  pentru script3 am folosit comenzile: 
- Uptime  timpul de functionare
- Hostname  numele gazdei
- Ifconfig - network private ip
- Whoami  numele utilizatorului current

Ex.4  pentru script4 am rulat comanda sudo apt update && sudo apt upgrade -y
-y  pentru a nu mai intreba  yes/no , si a instala direct actualizarile

Ex.5  pentru script5 am rulat in ordine urmatoarele comenzi : 
- touch  pentru a crea un fisier gol
- chmod +x pentru a adauga permisiuni de executie
- echo "#!/bin/bash" > "$script_name"  pentru a scrie intepretorul de bash pe prima linie a scriptului
- vim  pentru a deschide fisierul create
Aceste comenzi automatizeaza crearea unui script bash si il deschide.
Pentru executare se foloseste comanda ./script5.sh nume.sh

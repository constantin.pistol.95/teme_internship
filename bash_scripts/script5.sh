#!/bin/bash

if [  "$#" -ne 1 ]; then 
	echo "this script requires one argument wich is the script name"
exit 1 
fi
script_name="$1"
touch "$script_name" 
chmod +x "$script_name"
echo "#!/bin/bash" > "$script_name"
vim "$script_name"
#!/bin/bash

file=$1

if [[ -d "$file" ]]; then
   echo "Fisierul este un director"
elif [[ -L "$file" ]]; then
   echo "Fisierul este un symlink"
elif [[ -x "$file" ]]; then
   echo "Fisierul este executabil"
elif [[ -f "$file" ]]; then
   echo "Fisierul este un fisier normal"
fi

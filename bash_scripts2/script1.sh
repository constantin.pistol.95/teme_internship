#!/bin/bash

locatie=$1
extensie=$2
new_extensie=$3

for f in $locatie/*$extensie; do
    mv -- "$f" "${f%$extensie}$new_extensie"
done

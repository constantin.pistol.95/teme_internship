### script1.sh

1. Sa se scrie un script bash care redenumeste toate fisierele dintr-un folder dintr-o extensie in alta.
Scriptul primeste 3 argumente:
- locatie folder
- extensie care sa fie redenumita
- noua extensie

De exemplu:

./rename_script.sh  "/home/user/logs/"  ".log"  ".txt"

Scriptul va redenumi toate fisierele din folder-ul "/home/user/logs/" din ".log" in ".txt"

Explicatie cod:
- parcurge fisierele dintr-o locatie, cautandu-le doar pe cele cu extensia data ca parametru
- pentru fiecare fisier gasit executa comanda mv care redenumeste extensia in cea noua data ca parametru
- https://linuxize.com/post/how-to-rename-files-in-linux/
- pentru executare am folosit :
  - touch nume.log
  - ./script1.sh /vagrant .log .txt

### script2.sh

1. Sa se scrie un bash script care:
- Verifica daca un sistem este de tip Debian/Ubuntu sau CentOS
- Va rula comenzile de �update� si �upgrade� conforme sistemului respectiv
- Va instala automat �git� si �vim�

Explicatie cod:
- https://askubuntu.com/questions/459402/how-to-know-if-the-running-platform-is-ubuntu-or-centos-with-help-of-a-bash-scri/651124#651124
 - in functie de distrbutia linuxului exista un fisier ce spune ce sistem de operare este instalat, adica /etc/redhat-release este prezent doar pentru 'Centos' sau 'Redhat', iar cand este prezent fisierul etc/lsb-release sistemul de operare este ubuntu 
- pentru sistemele de operare de tip centos pachetele se instaleaza folosind yum
- pentru sitemele de operare ubuntu pachetele se intaleaza folosind apt-get
- pentru executare am folosit
  - script3.sh

### script3.sh

2. Sa se scrie un bash script care:
Verifica daca un �group� denumit �devops� exista pe sistem:
    - daca exista sa se ruleze : echo "DevOps group exists!"
	- altfel sa se creeze un group cu numele �devops�

Verifica daca un user �engineer� exista pe sistem:
    - Daca exista sa se ruleze: echo "User engineer already exists!"
	- altfel, sa se creeze user-ul si sa fie adaugat in group-ul �devops�

Explicatie cod:
- am verificat daca un grup denumit devops exista in fisierul /etc/group
- daca exista arata un mesaj , daca nu il creeaza
- verifica daca userul engineer exista pe sistem, pentru a verifica am folosit comanda id 
- daca exista arata un mesaj, daca nu il creeeaza si il adauga in grupul devops
- pentru executare am folosit:
  - ./script3.sh
- pentru verificare am folosit
  - id engineer , ar trebui sa afiseze " uid=1002(engineer) gid=1003(engineer) groups=1003(engineer),1002(devops) "
  - ls -l /home , vom vedea ca exista un folder cu numele userului creat

### script4.sh

3. Sa se scrie un bash script care:
- Primeste ca si argument numele la un fisier sau folder
- Sa faca �echo� daca aceasta este un fisier, folder, symlink etc. (tipul de fisier)
- Sa faca �echo� daca are permisii de executabil

Explicatie cod:
- arata ce tip este un fisier primit ca argument si daca are permisiuni de executie
- am folosit pentru acest exercitiu urmatoarele 2 link uri
- https://www.tutorialspoint.com/unix/if-elif-statement.htm
- https://stackoverflow.com/questions/638975/how-do-i-tell-if-a-regular-file-does-not-exist-in-bash
- si am executat astfel
  - ./script4.sh script3.sh , afiseaza "fisierul este executabil" 
  - ./script4.sh test1.txt , afiseaza "fisierul este un fisier normal"
  - ./script4.sh /vagrant , afiseaza "fisierul este un director"
  - ./script4.sh /etc/resolv.conf , afiseaza "fisierul este un symlink"

### script5.sh

4. Sa se scrie un bash script care:
- Primeste ca si argument locatia la un folder/director
- Ne va arata cele mai mari 5 fisiere din folder-ul respective
 
Explicatie cod:
- primeste ca argument locatia folder si arata cele mai mari 5 fisiere din folderul respectiv
- https://www.tutorialspoint.com/find-the-largest-top-10-files-and-directories-on-a-linux
-  am testat astfel
   - ./script5.sh /var/log

### script6.sh

5. Sa se scrie un bash script care:
- Primeste un numar �N� ca si argument
- Se va duce pe �filesystem� N foldere in urma
Exemplu:

echo $(pwd)

 /home/user/folder1/folder2/folder3/folder4/folder5

./move.sh 3

echo $(pwd)

 /home/user/folder1/folder2/
 
 Explicatie cod:
- https://stackoverflow.com/questions/12198222/go-up-several-directories-in-linux/12198323#12198323
- primeste un numar N ca argument si schimba directorul cu N foldere in urma
- pentru a executa , 
  - source script6.sh 2
- https://stackoverflow.com/questions/255414/why-i-cant-change-directories-using-cd/21676651#21676651

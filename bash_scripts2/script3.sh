#!/bin/bash

group='devops'

if grep -q $group /etc/group
then
   echo "DevOps group exists!"
else
   echo "DevOps group does not exist!"
   sudo groupadd $group
fi

user="engineer"

if id -u "$user" >/dev/null 2>&1; then
  echo "User engineer already exists!"
else
  echo "User engineer does not exist!"
  sudo useradd -m $user
  sudo usermod -aG $group $user
fi


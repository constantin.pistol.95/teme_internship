#!/bin/bash

if [ -f /etc/redhat-release ]; then
  echo 'Centos'
  sudo yum update && sudo yum upgrade -y
  sudo yum install git -y
  sudo yum install vim -y
fi

if [ -f /etc/lsb-release ]; then
  echo 'Ubuntu'
  sudo apt-get update && sudo apt-get upgrade -y
  sudo apt-get install git -y
  sudo apt-get install vim -y
fi

